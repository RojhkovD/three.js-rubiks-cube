# README #

This is a simple three.js simulator of Rubik's cube.

You can control the rotation by pressing key buttons: "Up" - u, "Left" - l etc.

![demo.png](https://bitbucket.org/repo/7aGBb8/images/2893961106-demo.png)