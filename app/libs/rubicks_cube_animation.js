
          var scene, camera, renderer;
          var group = new THREE.Object3D();
          var proc = false; // no asynchronous processing
          var rot = 0;
          var angle = -6*Math.PI/180;
          var code = 0;
          var target = 0;
          var num = 0; // number of cube in rotating plane
          var temprx = []; // for coordinate changing
          var tempry = [];
          var temprz = [];
          var controlArrows = [];
          var key = [66,82,76,70,85,68];
          var i = 0;
          var v = 0;
          var time = 0;
          var container = document.createElement('div');
          container.id = "can";

          animation_box.appendChild(container);
    
          camera = new THREE.PerspectiveCamera(70,window.innerWidth/window.innerHeight,0.1,1000000);
          camera.position.set(600,500,300);
          

          scene  = new THREE.Scene();
          scene.add(camera);

           var cube_geo = new THREE.BoxGeometry(90,90,90);
          var cube_texture = new THREE.MeshFaceMaterial( [
                    new THREE.MeshPhongMaterial({color: 0xffff00}),
                     new THREE.MeshLambertMaterial({color: 0xffffff}),
                      new THREE.MeshPhongMaterial({color: 0xff0000}),
                       new THREE.MeshPhongMaterial({color: 0xff4b10}),
                        new THREE.MeshPhongMaterial({color: 0x00ff00}),
                         new THREE.MeshPhongMaterial({color: 0x0000ff}) ]);
          var cube_instance = new THREE.Mesh( cube_geo, cube_texture );
            
                  var cube = cube_instance.clone();
                  cube.position.set(-100,-100,-100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(0,-100,-100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(100,-100,-100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(-100,0,-100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(0,0,-100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(100,0,-100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(-100,100,-100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(0,100,-100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(100,100,-100);
                  scene.add(cube);

                  var cube = cube_instance.clone();
                  cube.position.set(-100,-100,0);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(0,-100,0);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(100,-100,0);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(-100,0,0);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(0,0,0);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(100,0,0);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(-100,100,0);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(0,100,0);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(100,100,0);
                  scene.add(cube);

                  var cube = cube_instance.clone();
                  cube.position.set(-100,-100,100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(0,-100,100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(100,-100,100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(-100,0,100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(0,0,100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(100,0,100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(-100,100,100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(0,100,100);
                  scene.add(cube);
                  var cube = cube_instance.clone();
                  cube.position.set(100,100,100);
                  scene.add(cube);

                  

          document.documentElement.addEventListener("keydown", function(event){
            main(event);
          }
          );
          window.addEventListener('resize', function(){
            renderer.setSize(window.innerWidth, window.innerHeight);
            camera.aspect = window.innerWidth / window.innerHeight;
            camera.updateProjectionMatrix();
          });
          
          var light = new THREE.PointLight( 0xffffff);
          light.position.set(600,500,300);
          scene.add( light );

                  renderer = new THREE.WebGLRenderer( { alpha: true, antialias: true } );

          renderer.setSize(window.innerWidth, window.innerHeight);
          renderer.shadowMapType = THREE.PCFSoftShadowMap;
          container.appendChild(renderer.domElement);

          controls =  new THREE.OrbitControls(camera, renderer.domElement);

          controlslight =  new THREE.OrbitControls(light, renderer.domElement);
           
          drawContols();

          animate();
          scramble(i);
          function animate(){
            requestAnimationFrame(animate);
            for(var u = 0;u<6;u++)
            controlArrows[u].lookAt(camera.position);
            switch(rot){
              case "R":
                
              //console.log("1");
                  group.rotateOnAxis(new THREE.Vector3(1,0,0), angle);
                if( -group.rotation.x >= 0.5 * Math.PI){ // rotation of 1 turn is = 0.25 * 2 pi radians
                   planeNumbersCount(rot); // now we have group childrens with numbers in our rotation plane
                   for(var i = 0;i<9;i++){ // coordinates offset on 90 degrees == 2 cube positions of coordinate swap
                      if(group.children[i].userData.num + 2 <= 7)
                          num = group.children[i].userData.num + 2;
                      else if(group.children[i].userData.num + 2 == 8)
                          num = 0;
                      else if(group.children[i].userData.num + 2 == 9)
                          num = 1;
                      else if(group.children[i].userData.num == undefined)
                          num = null;
                    for(var p = 0;p<9;p++)
                        if(num == group.children[p].userData.num){
                            temprx[i] = group.children[p].position.x;
                            tempry[i] = group.children[p].position.y;
                            temprz[i] = group.children[p].position.z;
                        }
                  }
                  rot = 0;
                  Detach();
                 num = 0;
                 group.rotation.x = 0;
                 proc = false;
              }
                break;
              case "L":
                   group.rotateOnAxis(new THREE.Vector3(-1,0,0), angle);
              if( group.rotation.x >= 0.5 * Math.PI){ // rotation of 1 turn is = 0.25 * 2 pi radians
                   planeNumbersCount(rot); // now we have group childrens with numbers in our rotation plane
                   for(var i = 0;i<9;i++){ // coordinates offset on 90 degrees == 2 cube positions of coordinate swap
                      if(group.children[i].userData.num + 2 <= 7)
                          num = group.children[i].userData.num + 2;
                      else if(group.children[i].userData.num + 2 == 8)
                          num = 0;
                      else if(group.children[i].userData.num + 2 == 9)
                          num = 1;
                      else if(group.children[i].userData.num == undefined)
                          num = null;
                    for(var p = 0;p<9;p++)
                        if(num == group.children[p].userData.num){
                            temprx[i] = group.children[p].position.x;
                            tempry[i] = group.children[p].position.y;
                            temprz[i] = group.children[p].position.z;
                            
                        }
                  }
                  rot = 0;
                  Detach();
                 num = undefined;
                 group.rotation.x = 0;
                 proc = false;
              }
                break;
              case "F":
               group.rotateOnAxis(new THREE.Vector3(0,0,1), angle);
                if( -group.rotation.z >= 0.5 * Math.PI){ // rotation of 1 turn is = 0.25 * 2 pi radians

                   planeNumbersCount(rot); // now we have group childrens with numbers in our rotation plane
                   for(var i = 0;i<9;i++){ // coordinates offset on 90 degrees == 2 cube positions of coordinate swap
                      if(group.children[i].userData.num + 2 <= 7)
                          num = group.children[i].userData.num + 2;
                      else if(group.children[i].userData.num + 2 == 8)
                          num = 0;
                      else if(group.children[i].userData.num + 2 == 9)
                          num = 1;
                      else if(group.children[i].userData.num == undefined)
                          num = null;
                    for(var p = 0;p<9;p++)
                        if(num == group.children[p].userData.num){
                            temprx[i] = group.children[p].position.x;
                            tempry[i] = group.children[p].position.y;
                            temprz[i] = group.children[p].position.z;
                            
                        }


                  }

                  rot = 0;
                  Detach();
                 num = undefined;
                 group.rotation.z = 0;
                 proc = false;
              }
                break;
              case "B":
               group.rotateOnAxis(new THREE.Vector3(0,0,-1), angle);
               if( group.rotation.z >= 0.5 * Math.PI){ // rotation of 1 turn is = 0.25 * 2 pi radians
                   planeNumbersCount(rot); // now we have group childrens with numbers in our rotation plane
                   for(var i = 0;i<9;i++){ // coordinates offset on 90 degrees == 2 cube positions of coordinate swap
                      if(group.children[i].userData.num + 2 <= 7)
                          num = group.children[i].userData.num + 2;
                      else if(group.children[i].userData.num + 2 == 8)
                          num = 0;
                      else if(group.children[i].userData.num + 2 == 9)
                          num = 1;
                      else if(group.children[i].userData.num == undefined)
                          num = null;
                    for(var p = 0;p<9;p++)
                        if(num == group.children[p].userData.num){
                            temprx[i] = group.children[p].position.x;
                            tempry[i] = group.children[p].position.y;
                            temprz[i] = group.children[p].position.z;
                            
                        }


                  }

                  rot = 0;
                  Detach();
                 num = undefined;
                 group.rotation.z = 0;
                 proc = false;
              }
                break;
              case "U":
               group.rotateOnAxis(new THREE.Vector3(0,1,0), angle);

               if( -group.rotation.y >= 0.5 * Math.PI){ // rotation of 1 turn is = 0.25 * 2 pi radians
                   planeNumbersCount(rot); // now we have group childrens with numbers in our rotation plane
                   for(var i = 0;i<9;i++){ // coordinates offset on 90 degrees == 2 cube positions of coordinate swap
                      if(group.children[i].userData.num + 2 <= 7)
                          num = group.children[i].userData.num + 2;
                      else if(group.children[i].userData.num + 2 == 8)
                          num = 0;
                      else if(group.children[i].userData.num + 2 == 9)
                          num = 1;
                      else if(group.children[i].userData.num == undefined)
                          num = null;
                    for(var p = 0;p<9;p++)
                        if(num == group.children[p].userData.num){
                            temprx[i] = group.children[p].position.x;
                            tempry[i] = group.children[p].position.y;
                            temprz[i] = group.children[p].position.z;
                            
                        }


                  }

                  rot = 0;
                  Detach();
                 num = undefined;
                 group.rotation.y = 0;
                 proc = false;
              }
                break;
              case "D":
               group.rotateOnAxis(new THREE.Vector3(0,-1,0), angle);
               if( group.rotation.y >= 0.5 * Math.PI){ // rotation of 1 turn is = 0.25 * 2 pi radians
                   planeNumbersCount(rot); // now we have group childrens with numbers in our rotation plane
                   for(var i = 0;i<9;i++){ // coordinates offset on 90 degrees == 2 cube positions of coordinate swap
                      if(group.children[i].userData.num + 2 <= 7)
                          num = group.children[i].userData.num + 2;
                      else if(group.children[i].userData.num + 2 == 8)
                          num = 0;
                      else if(group.children[i].userData.num + 2 == 9)
                          num = 1;
                      else if(group.children[i].userData.num == undefined)
                          num = null;
                    for(var p = 0;p<9;p++)
                        if(num == group.children[p].userData.num){
                            temprx[i] = group.children[p].position.x;
                            tempry[i] = group.children[p].position.y;
                            temprz[i] = group.children[p].position.z;
                            
                        }


                  }

                  rot = 0;
                  Detach();
                 num = undefined;
                 group.rotation.y = 0;
                 proc = false;
              }
                break;
      }

            
            renderer.render(scene,camera);
            controls.update();
            controlslight.update();
            
          }



  function planeNumbersCount(arg){
            var globalfunction;
            var tempx = 0;
            var tempy = 0;
            var tempz = 0;
            switch(arg){
              case "R":
              
              for(var u = 0;u<9;u++){
                        if(group.children[u].position.x == group.children[u].position.y && group.children[u].position.z == 100){
                          
                          group.children[u].userData.num = 0;
                          tempx = group.children[u].position.x;
                          tempy = group.children[u].position.y;
                          tempz = group.children[u].position.z;
                      }
                     }
                     
                      tempz -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 1;
                          break;
                        }
                        }
                      
                      tempz -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 2;
                          break;
                        }
                        }
                        
                      tempy -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 3;
                          break;
                        }
                        }
                        
                      tempy -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 4;
                          break;
                        }
                      }
                      
                      tempz +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 5;
                          break;
                        }
                        }
                        
                      tempz +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 6;
                          break;
                        }
                        }
                      tempy +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 7;
                        break;
                      }
                     }

              case "L":
                  for(var u = 0;u<9;u++){
                        if(group.children[u].position.x == group.children[u].position.y && group.children[u].position.z == 100){

                          group.children[u].userData.num = 0;
                          tempx = group.children[u].position.x;
                          tempy = group.children[u].position.y;
                          tempz = group.children[u].position.z;
                          }
                        }
                          tempz -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 1;
                          break;
                        }
                        }
                      tempz -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 2;
                          break;
                        }
                        }
                      tempy +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 3;
                          break;
                        }
                        }
                      tempy +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 4;
                          break;
                        }
                        }
                      tempz +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 5;
                          break;
                        }
                        }
                      tempz +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 6;
                          break;
                        }
                        }
                      tempy -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.z == tempz && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 7;
                          break;
                        }
                       }
                     break;
              case "F":
                  for(var u = 0;u<9;u++){
                        if(group.children[u].position.x == group.children[u].position.z && group.children[u].position.y == 100){
                           
                          group.children[u].userData.num = 0;
                          tempx = group.children[u].position.x;
                          tempy = group.children[u].position.y;
                          tempz = group.children[u].position.z;
                        }
                      }
                      tempy -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 1;
                          break;
                        }
                        }
                      tempy -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 2;
                          break;
                        }
                        }
                      tempx -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 3;
                          break;
                        }
                        }
                      tempx -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 4;
                          break;
                        }
                        }
                      tempy +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 5;
                          break;
                        }
                        }
                      tempy +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 6;
                          break;
                        }
                        }
                      tempx +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 7;
                        break;
                      }
                      }
                     break;
               case "B":
                  for(var u = 0;u<9;u++){
                        if(group.children[u].position.x == group.children[u].position.z && group.children[u].position.y == 100){
                          group.children[u].userData.num = 0;
                          tempx = group.children[u].position.x;
                          tempy = group.children[u].position.y;
                          tempz = group.children[u].position.z;
                        }
                      }
                          tempy -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 1;
                          break;
                        }
                        }
                      tempy -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 2;
                          break;
                        }
                        }
                      tempx +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 3;
                          break;
                        }
                        }
                      tempx +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 4;
                          break;
                        }
                        }
                      tempy +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 5;
                          break;
                        }
                        }
                      tempy +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 6;
                          break;
                        }
                        }
                      tempx -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.y == tempy){
                            group.children[t].userData.num = 7;
                          break;
                        }
                        }
                     break;
              case "U":
                 for(var u = 0;u<9;u++){
                        if(group.children[u].position.x == group.children[u].position.y && group.children[u].position.z == 100){
                          group.children[u].userData.num = 0;
                          tempx = group.children[u].position.x;
                          tempy = group.children[u].position.y;
                          tempz = group.children[u].position.z;
                            }
                          }
                      tempx -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 1;
                          break;
                        }
                        }
                      tempx -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 2;
                          break;
                        }
                        }
                      tempz -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 3;
                          break;
                        }
                        }
                      tempz -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 4;
                          break;
                        }
                        }
                      tempx +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 5;
                          break;
                        }
                        }
                      tempx +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 6;
                          break;
                        }
                        }
                      tempz +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 7;
                      break;
                    }
                    }
                     break;
              case "D":
              for(var u = 0;u<9;u++){
                        if(group.children[u].position.x == group.children[u].position.y && group.children[u].position.z == 100){
                          group.children[u].userData.num = 0;
                          tempx = group.children[u].position.x;
                          tempy = group.children[u].position.y;
                          tempz = group.children[u].position.z;
                        }
                      }
                          tempx +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 1;
                          break;
                        }
                        }
                      tempx +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 2;
                          break;
                        }
                        }
                      tempz -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 3;
                          break;
                        }
                        }
                      tempz -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 4;
                          break;
                        }
                        }
                      tempx -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 5;
                          break;
                        }
                        }
                      tempx -=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 6;
                          break;
                        }
                        }
                      tempz +=100;
                    for(var t = 0;t<9;t++){
                        if(group.children[t].position.x == tempx && group.children[t].position.z == tempz){
                            group.children[t].userData.num = 7;
                          break;
                        }
                        }
                     break;
       }  // switch

                          tempx = 0;
                          tempy = 0;
                          tempz = 0;

   }  //function
          function scramble(i){
            if(i == 0)
              time = new Date();
            if(i<25){
                code = Math.round(Math.random() * 5);
                 main(undefined, key[code]);
               i++;
           setTimeout(function(){setTimeout(scramble(i), 50);}, 300);
          }
      else
        return 0;
  }

          function main(event, code){
            if(proc == false){
              if(event)
              var arg = event.keyCode;
            else {
              var arg = code;
              angle = -9*Math.PI/180 + 0.000003;
            }
            switch(arg){
              case 82: //R
                  proc = true;
                   rot = "R";
                  for(var i = 0, v = 9;v>0;i++){
                  
                    if(scene.children[i].position.x == 100){

                        v--;
                       THREE.SceneUtils.attach(scene.children[i], scene, group);
                       i--;
                     }
                  }
                  
                  scene.add(group);
                  break;
              case 76: //L
                    proc = true;
              rot = "L";
                   for(var i = 0, v = 9;v>0;i++){
                  
                    if(scene.children[i].position.x == -100){

                        v--;
                       THREE.SceneUtils.attach(scene.children[i], scene, group);
                       i--;
                     }
                  }
                  scene.add(group);
                  break;
              case 70: //F
                    proc = true;
              rot = "F";
                    for(var i = 0, v = 9;v>0;i++){
                    if(scene.children[i].position.z == 100){
                        v--;
                       THREE.SceneUtils.attach(scene.children[i], scene, group);
                       i--;
                     }
                  }
                  scene.add(group);
                  break;
              case 66: //B
                      proc = true;
              rot = "B";
                     for(var i = 0, v = 9;v>0;i++){
                  
                    if(scene.children[i].position.z == -100){

                        v--;
                       THREE.SceneUtils.attach(scene.children[i], scene, group);
                       i--;
                     }
                  }
                  scene.add(group);
                  break;
              case 85: //U
                      proc = true;
              rot = "U";
                     for(var i = 0, v = 9;v>0;i++){
                  
                    if(scene.children[i].position.y == 100){

                        v--;
                       THREE.SceneUtils.attach(scene.children[i], scene, group);
                       i--;
                     }
                  }
                  scene.add(group);
                  break;
              case 68: //D
                      proc = true;
              rot = "D";
                    for(var i = 0, v = 9;v>0;i++){
                  
                    if(scene.children[i].position.y == -100){

                      v--;
                       THREE.SceneUtils.attach(scene.children[i], scene, group);
                      i--;
                     }
                  }
                  scene.add(group);
                  break;
            }
          }
                  code = 0;
          }
          function drawContols(){
              text =["Right","Left","Front","Back","Up","Down"];
              var canvas = [];
              var context = 0;
              var texture = 0;
              var material = 0;
              for(var y = 0;y<6;y++){
                canvas[y] = document.createElement('canvas');
                context = canvas[y].getContext('2d');
                context.font = "Bold 40px Impact";
                w_start = 0;
                for(var x = 0;x<text[y].length;x++){
                  if(x!=0){
                    context.fillStyle = "rgba(0,0,0,0.75)";
                  }else{
                    context.fillStyle = "rgba(255,0,0,0.95)";
                  }
                  context.fillText(text[y][x], w_start, 50);
                  w_start += context.measureText(text[y][x]).width;
                }
                  texture = new THREE.Texture(canvas[y]);
                  material = new THREE.MeshBasicMaterial( {map: texture, side:THREE.DoubleSide } );
                  material.transparent = true;
                  texture.needsUpdate = true;
                  controlArrows[y] = new THREE.Mesh(
                        new THREE.PlaneGeometry(canvas[y].width, canvas[y].height), material);
              }
              controlArrows[0].position.set(500,0,0);
              controlArrows[1].position.set(-500,0,0);
              controlArrows[2].position.set(0,0,500);
              controlArrows[3].position.set(0,0,-500);
              controlArrows[4].position.set(0,500,0);
              controlArrows[5].position.set(0,-500,0);
              for(var u = 0;u<6;u++){
                scene.add(controlArrows[u]);
                controlArrows[u].lookAt(camera.position);
                controlArrows[u].needsUpdate = true;
              }
              
             
           
          }

          function Detach(){
            for(var u = 0, j = 27;u<9 && j<36;u++,j++){
                 THREE.SceneUtils.detach(group.children[0],group,scene);  
                      scene.children[j].position.x = temprx[u];
                      scene.children[j].position.y = tempry[u];
                      scene.children[j].position.z = temprz[u];
                 var x = scene.children[j].rotation.x;
                  var y = scene.children[j].rotation.y;
                  var z = scene.children[j].rotation.z;

                          if(z > 0 && z < 1 || z < 0 && z > -1){ 
                    scene.children[j].rotation.z = 0;
                  }
                  if(z > -1.6 && z < -1 || z < -1.6 && z > -2 ){
                    scene.children[j].rotation.z = -Math.PI/2;
                    }
                  if(z < 1.6 && z > 1 || z > 1.6 && z < 2 ){
                    scene.children[j].rotation.z = Math.PI/2;
                    }
                    if(z < -3.14 && z > -4 || z > -3.14 && z < -2 ){
                        scene.children[j].rotation.z = Math.PI;
                      }
                  if(z >  3.14 && z< 4 || z < 3.14 && z > 2.5 ){
                        scene.children[j].rotation.z = Math.PI;
                      }
                 if(x > 0 && x < 1 || x < 0 && x > -1){ // delete rotation offset in local plane
                        scene.children[j].rotation.x = 0;
                      }
                  if(x > -1.6 && x < -1 || x < -1.6 && x > -2 ){
                        scene.children[j].rotation.x = -Math.PI/2;
                      }
                  if(x < 1.6 && x > 1 || x > 1.6 && x < 2){
                    scene.children[j].rotation.x = Math.PI/2;
                  } 
                  if(x < -3.14 && x > -4 || x > -3.14 && x < -2 ){
                        scene.children[j].rotation.x = Math.PI;
                      }
                  if(x >  3.14 && x < 4 || x < 3.14 && x > 2.5 ){

                        scene.children[j].rotation.x = Math.PI;
                      }
                if(y > 0 && y < 1 || y < 0 && y > -1){ // delete rotation offset in local plane
                        scene.children[j].rotation.y = 0;
                      }
                  if(y > -1.6 && y < -1 || y < -1.6 && y > -2 ){
                        scene.children[j].rotation.y = -Math.PI/2;
                      }
                  if(y < 1.6 && y > 1 || y > 1.6 && y < 2){
                    scene.children[j].rotation.y = Math.PI/2;
                  } 
                  if(y < -3.14 && y > -4 || y > -3.14 && y < -2 ){
                        scene.children[j].rotation.y = Math.PI;
                      }
                  if(y >  3.14 && y < 4 || y < 3.14 && y > 2.5 ){
                        scene.children[j].rotation.y = Math.PI;
                      }


               }

          }













































